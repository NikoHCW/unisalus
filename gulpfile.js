// Gulp.js configuration
'use strict';

const

  // source and build folders
  dir = {
    src           : 'src/',
    build         : 'wordpress/wp-content/themes/unisalus/',
    //proxyUrl      : 'http://ar4fm.test',
    baseUrlImage  : 'wp-content/themes/unisalus/'   /* for Postcss assets */
  },

  // Gulp and plugins
	gulp          = require('gulp'),
	gutil         = require('gulp-util'), 
	newer         = require('gulp-newer'), //copia php da src a dest
	imagemin      = require('gulp-imagemin'),
	sass          = require('gulp-sass'),
	postcss       = require('gulp-postcss'),
	concat        = require('gulp-concat'),
	uglify        = require('gulp-uglify'),
	jslint        = require('gulp-jslint'),
	npmFiles 	  	= require('gulp-npm-files'),
	rename 		  	= require('gulp-rename');

// Browser-sync
var browsersync = false;

const path = {
  php: {
    src         : dir.src + '**/*.php',
    build       : dir.build
  },
  stylecss: {
    src         : dir.src + 'style.css',
    build       : dir.build,
  },
  fonts: {
    src			: dir.src + 'assets/fonts/**/*',
    build		: dir.build + 'assets/fonts/'
  },
  images: {
    src         : dir.src + 'assets/images/**/*',
    build       : dir.build + 'assets/images/'
  },
  js: {
    src         : dir.src + 'assets/js/**/*',
    build       : dir.build + 'assets/js/',
    filename    : 'scripts.js'
  }, 
  vendors: {
    src         : dir.src + 'assets/vendors/**/*',
    build       : dir.build + 'assets/vendors/'
  },
  scss: {
    src         : dir.src + 'assets/scss/main.scss', //file in cui metto import di tutti i scss
    watch       : dir.src + 'assets/scss/**/*',
    build       : dir.build + 'assets/css/',
    sassOpts: {
      outputStyle     : 'nested',
      imagePath       : dir.build + 'assets/images/',
      precision       : 3,
      errLogToConsole : true,
    },
    processors: [
      require('postcss-assets')({
        loadPaths: ['images/'],
        basePath: dir.build,
        baseUrl: dir.baseUrlImage
      }),
      require('autoprefixer')({
        browsers: ['last 2 versions', '> 2%']
      }),
      require('cssnano')
    ]
  }
};

// Browsersync options
const syncOpts = {
  proxy       : dir.proxyUrl,
  files       : dir.build + '**/*',
  open        : true,
  notify      : false,
  ghostMode   : false,
  ui: {
    port: 8001
  }
};


// copy PHP files
gulp.task('php', function() {
  return gulp.src(path.php.src)
    .pipe(newer(path.php.build))
    .pipe(gulp.dest(path.php.build));
});

// copy Style.css
gulp.task('stylecss', function() {
  return gulp.src(path.stylecss.src)
    .pipe(newer(path.stylecss.build))
    .pipe(gulp.dest(path.stylecss.build));
});


// copy FONTS files
gulp.task('fonts', function () {
  return gulp.src(path.fonts.src)
    .pipe(newer(path.fonts.build))
    .pipe(gulp.dest(path.fonts.build));
});

// copy vendors files
gulp.task('vendors', function() {
  return gulp.src(path.vendors.src)
    .pipe(newer(path.vendors.build))
    .pipe(gulp.dest(path.vendors.build));
});

// image processing
gulp.task('images', function() {
  return gulp.src(path.images.src)
    .pipe(newer(path.images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(path.images.build));
});



// SCSS processing
gulp.task('scss', gulp.series('images', function() {
  return gulp.src(path.scss.src)
    .pipe(sass(path.scss.sassOpts))
    .pipe(postcss(path.scss.processors))
    .pipe(gulp.dest(path.scss.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
}));


// JavaScript processing
gulp.task('js', function() {

  return gulp.src(path.js.src)
    .pipe(concat(path.js.filename))
    .pipe(uglify())
    .pipe(gulp.dest(path.js.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());

});


// run all tasks
gulp.task('build', gulp.parallel('stylecss','php', 'vendors', 'fonts', 'scss', 'js'));


// browser-sync
gulp.task('browsersync', function() {
  if (browsersync === false) {
    browsersync = require('browser-sync').create();
    browsersync.init(syncOpts);
  }
});


// watch for file changes
gulp.task('watch', gulp.parallel('browsersync', function() {

  // page changes
  gulp.watch(path.php.src, gulp.series('php')).on('change', browsersync ? browsersync.reload : {});
  // image changes
  gulp.watch(path.images.src, gulp.series('images'));
  // CSS changes
  gulp.watch(path.scss.watch, gulp.series('scss')).on('change', browsersync ? browsersync.reload : {});
  // JavaScript main changes
  gulp.watch(path.js.src, gulp.series('js')).on('change', browsersync ? browsersync.reload : {});

}));


// default task
gulp.task('default', gulp.series('build', 'watch'));
