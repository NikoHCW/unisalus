<?php
/**
 * Template Name: TPL Contatti
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url('<?php the_post_thumbnail_url('full'); ?>')">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>

					<!-- Section: Have Any Question -->
					<section class="divider">
						<div class="container pt-60 pb-60">
							<div class="section-content">
								<div class="row">
									<div class="col-sm-12 col-md-4">
										<div class="contact-info text-center">
											<?php if (get_field('phone','option')): ?>
												<i class="fa fa-phone font-36 mb-10 text-theme-colored"></i>
												<?php if (get_field('titolo_telefono','options')): ?>
													<h4><?php the_field('titolo_telefono','options') ?></h4>
												<?php endif ?>
												<a href="tel:<?php the_field('phone','option') ?>"><h6 class="text-gray"><?php the_field('phone','option') ?></h6></a>
											<?php endif ?>
											
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="contact-info text-center">
											<?php if (get_field('address','option')): ?>
												<i class="fa fa-map-marker font-36 mb-10 text-theme-colored"></i>
												
												<?php if (get_field('titolo_indirizzo','options')): ?>
													<h4><?php the_field('titolo_indirizzo','options') ?></h4>
												<?php endif ?>
												<a href="https://maps.google.com/?q=<?php the_field('address','option') ?>"><h6 class="text-gray"><?php the_field('address','option') ?></h6></a>
											<?php endif ?>
											
										</div>
									</div>
									<div class="col-sm-12 col-md-4">
										<div class="contact-info text-center">
											
											<?php if (get_field('email','option','options')): ?>
												<i class="fa fa-envelope font-36 mb-10 text-theme-colored"></i>
												<?php if (get_field('titolo_email','options')): ?>
													<h4><?php the_field('titolo_email','options') ?></h4>
												<?php endif ?>
												<a href="mailto:<?php print antispambot(the_field('email','option')) ?>"><h6 class="text-gray"><?php print antispambot(the_field('email','option')) ?></h6></a>	
											<?php endif ?>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>

					<!-- Divider: Contact -->
					<section class="divider bg-lighter">
						<div class="container">
							<div class="row pt-30">
								<div class="col-md-7">
									<h3 class="line-bottom mt-0 mb-30"><?php the_field('titolo_form','options') ?></h3>

									<!-- Contact Form -->
									<?php if (get_field('form_contatti','option')): ?>
										
										<div id="contact_form" class="main-form">
											<?php echo do_shortcode(get_field('form_contatti','option')) ?>
										</div>
									<?php endif ?>
									
								</div>
								<div class="col-md-5">

									<?php the_field('maps', 'option') ?>

								</div>
							</div>
						</div>
					</section>
				</div>
				<!-- end main-content -->
			</article>

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
