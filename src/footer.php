<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package starter
 */

?>

<!-- Footer -->
<footer id="footer" class="footer bg-white">
	<div class="container pt-70 pb-40">
		<div class="row border-bottom-black">
			<div class="col-sm-6 col-md-3">
				<div class="widget">
					<div class="mt-10 mb-20"><?php the_custom_logo() ?></div>
					<p><?php the_field('footer_text','option') ?></p>
					<ul class="list-inline mt-5">
						<li class="m-0 pl-10 pr-10"> <i class="fa fa-map-marker text-theme-colored mr-5"></i> <a class="text-gray" href="<?php the_field('footer_address_link','option') ?>"><?php the_field('footer_address','option') ?></a> </li>

						<li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored mr-5"></i> <a class="text-gray" href="tel:<?php the_field('footer_phone','option') ?>"><?php the_field('footer_phone','option') ?></a> </li>

						<li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="mailto:<?php print antispambot(get_field('footer_email','option')) ?>"><?php print antispambot(get_field('footer_email','option')) ?></a> </li>
						
					</ul>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="widget">
					<h5 class="widget-title line-bottom">I nostri servizi</h5>
					<?php 
					$args = array(
						'post_type' => 'servizi',
						'posts_per_page' => '-1',
						'post__not_in' => array(get_the_ID())

					);
					$the_query = new WP_Query( $args );
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<div class="list-border py-10">
							<a href="<?php the_permalink() ?>"><?php the_title() ?></a>
						</div>


					<?php endwhile;
			 // Ripristina Query & Post Data originali
					wp_reset_query();
					wp_reset_postdata(); ?>


				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="widget">
					<h5 class="widget-title line-bottom">News</h5>
					<div class="latest-posts">
						<?php
					          //ultime 5 news
						$args= array(
							'post_type' => 'post',
							'posts_per_page' => 2,
						);
						$the_query = new WP_Query( $args );

					            // Il Loop
						while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<article class="post media-post clearfix pb-0 mb-10 footer-news flex-c">

								<a href="<?php the_permalink(); ?>" class="post-thumb">
									<?php if (get_the_post_thumbnail()){
										the_post_thumbnail('thumb');
									}else{
										?><img src="<?php bloginfo('template_directory') ?>/assets/images/placeholder-100x100.jpg');"> <?php
									}
									?>
								</a>
								<div class="post-right">

									<h5 class="post-title mt-0 mb-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
									<p class="post-date font-12"><?php echo get_the_date() ?></p>
									
								</div>
							</article>
						<?php endwhile;

				            // Ripristina Query & Post Data originali
						wp_reset_query();
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="widget">
					<h5 class="widget-title line-bottom">Orari</h5>
					<div class="opening-hours">
						<ul class="list-border">
							<?php if( have_rows('orario', 'option') ): 
								while ( have_rows('orario', 'option') ) : the_row(); ?>
									<li class="clearfix"> <span> <?php the_sub_field('days', 'option'); ?> :  </span>
										<div class="value pull-right flip"> <?php the_sub_field('time_open', 'option'); ?> - <?php the_sub_field('time_close', 'option'); ?> </div>
									</li>
								<?php endwhile;
							endif; ?>
							<li class="clearfix"> <span> <?php the_field('day_close', 'option'); ?> : </span>
								<div class="value pull-right flip"> Chiuso </div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div id="fb-root"></div>
				<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v4.0"></script>
				<?php dynamic_sidebar( 'footer-1' ); ?>

			</div>
		</div>
	</div>
	<div class="footer-bottom bg-black-222">
		<div class="container pt-10 pb-0">
			<div class="row">
				<div class="col-md-6 sm-text-center">
					<p class="font-13 text-black-777 m-0"><?php the_field('footer_copy','option') ?></p>
				</div>
				<div class="col-md-6 text-right flip sm-text-center">
					<div class="widget no-border m-0">
						<?php if( have_rows('footer_social', 'options') ): ?>
							<ul class="styled-icons icon-dark icon-circled icon-sm">
								<?php while ( have_rows('footer_social', 'options') ) : the_row(); ?>
									<li><a href="<?php the_sub_field('link_social','options') ?>"><i class="fa fa-<?php the_sub_field('social','options') ?>"></i></a></li>
								<?php endwhile; ?><!-- /while social_med-->
							</ul>
								<?php endif; ?><!--/if social_med-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<!-- end wrapper -->
	<?php wp_footer(); ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			stickyheader();
		});
	</script>
</body>
</html>

<?php 
