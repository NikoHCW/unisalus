<?php
/**
 * Template Name: TPL Servizi
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<?php 
					if(get_the_post_thumbnail() == ""){
							//$bg = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
						$bg = get_bloginfo('template_directory') . '/assets/images/doc.jpg';
					} else {
						$bg = get_the_post_thumbnail_url(get_the_ID(),'full') ;
					}
					?>
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php echo $bg; ?>)">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>

					<section id="doctors">
						<div class="container">
							<div class="row">
								<?php
         						
								$args= array(
									'post_type' => 'servizi',
									'posts_per_page' => '-1',
									'orderby' => 'menu_order',
									'order'=> 'ASC'
								);
								$the_query = new WP_Query( $args );
          							  // Il Loop
								while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

									<div class="col-xs-12 col-sm-6 col-md-4">
										<div class="item">
											<div class="p-20 card-img">
												<?php 
												if ( '' != get_the_post_thumbnail() ) {
													the_post_thumbnail('blog_thumb');
												} else {
													?><img src="<?php bloginfo('template_directory') ?>/assets/images/blog_thumb-placeholder.jpg');"> <?php 
												}
												?>
												<h3><?php the_title(); ?></h3>
												<p class=""><?php echo excerpt(20); ?></p>
												<a href="<?php the_permalink() ?>" class="btn btn-flat btn-theme-colored mt-15 text-theme-color-2">Scopri di più</a>                
											</div>
										</div>    
									</div>


								<?php endwhile;
           					 // Ripristina Query & Post Data originali
								wp_reset_query();
								wp_reset_postdata(); ?>
							</div>
						</div>
					</section>    <!-- /Doctors Section:-->



				</div>
				<!-- end main-content -->
			</article>

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
