<?php
/**
 * Template Name: TPL Home
 */

get_header(); ?>



<!-- Start main-content -->
<div class="main-content home-page">
  <?php while ( have_posts() ) : the_post(); ?>

    <!-- Section: home -->
    <?php if( have_rows('hero_slider') ): ?>

      <section id="home" class="divider parallax layer-overlay overlay-white-5">
       <div class="hero-slider">
        <?php while ( have_rows('hero_slider') ) : the_row();

         if ( get_sub_field( 'sfondo' ) ) : 

          $image = get_sub_field('sfondo');   
          $size = 'slider-thumb';
          $thumb = $image['sizes'][ $size ]; ?>

          <div class="display-table hero-bg" style="background-image: url(<?php echo $thumb ?>);">
            <?php endif; ?>
            <div class="overlay-bg">
              <div class="container pt-100 pb-100">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1">
                    <div class="bg-white-transparent text-center pt-20 pb-50 outline-border" >
                      <?php if ( get_sub_field( 'titolo' ) ) : ?>
                      <h1 class="text-theme-colored2 text-uppercase font-54"><?php the_sub_field('titolo') ?> </h1>
                      <?php endif; ?>
                      <div class="lead"><?php the_sub_field('sottotitolo') ?></div>
                      <?php if ( get_sub_field( 'link_btn' ) && get_sub_field( 'link_btn' )) : ?>
                      <a class="btn btn-colored btn-theme-colored btn-lg smooth-scroll-to-target mt-15" href="<?php the_sub_field('link_btn') ?>"><?php the_sub_field('label_btn') ?></a>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
      </div><!-- /.hero_slider-->
    </section>
  <?php endif; ?>

  <!-- Section: home-boxes -->
  <section>
    <div class="container">
      <div class="section-content">
        <div class="row equal-height-inner" style="margin-top:-150px">
          <?php if( have_rows('box') ): 

          while( have_rows('box') ): the_row(); ?>
          <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1">
            <div class="sm-height-auto bg-theme-colored">
              <div class="text-center pt-30 pb-30 pb-sm-30">
                <div class="icon-box text-center">
                  <div class="icon bg-silver-light icon-circled" href="#"> <i class="flaticon-medical-blood8 text-theme-colored"></i> </div>
                  <?php if (get_sub_field('titolo_box1')) : ?>
                  <h3 class="text-white"><?php the_sub_field('titolo_box1') ?></h3>
                  <?php endif; ?>
                  <?php if (get_sub_field('sub_box1')) : ?>
                  <p class="text-white"><?php the_sub_field('sub_box1') ?></p>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay2">
            <div class="sm-height-auto bg-theme-colored-darker3">
              <div class="text-center pt-30 pb-30 pb-sm-30">
                <div class="icon-box text-center">
                  <div class="icon bg-silver-light icon-circled" href="#"> <i class="flaticon-medical-medical45 text-theme-colored"></i> </div>
                  <?php if (get_sub_field('titolo_box2')) : ?>
                  <h3 class="text-white"><?php the_sub_field('titolo_box2') ?></h3>
                  <?php endif; ?>
                  <?php if (get_sub_field('sub_box2')) : ?>
                  <p class="text-white"><?php the_sub_field('sub_box2') ?></p>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay3">
            <div class="sm-height-auto bg-theme-colored-darker5">
              <div class="text-center pt-30 pb-30 pb-sm-30">
                <div class="icon-box text-center">
                  <div class="icon bg-silver-light icon-circled" href="#"> <i class="flaticon-medical-pill text-theme-colored"></i> </div>
                  <?php if (get_sub_field('titolo_box3')) : ?>
                  <h3 class="text-white"><?php the_sub_field('titolo_box3') ?></h3>
                  <?php endif; ?>
                  <?php if (get_sub_field('sub_box3')) : ?>
                  <p class="text-white"><?php the_sub_field('sub_box3') ?></p>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay4">
            <div class="sm-height-auto bg-theme-colored-darker9">
              <div class="text-center pt-30 pb-30 pb-sm-30">
                <div class="icon-box text-center">
                  <div class="icon bg-silver-light icon-circled" href="#"> <i class="fa fa-phone  text-theme-colored"></i> </div>
                  <?php if (get_sub_field('titolo_box4') && get_sub_field('telefono')) : ?>
                  <a href="tel:+39<?php the_sub_field('telefono') ?>" class="text-cta">
                    <h3 class="text-white"><?php the_sub_field('titolo_box4') ?> <?php the_sub_field('telefono') ?></h3>
                  </a>
                  <?php endif; ?>
                  <?php if (get_sub_field('sub_box4')) : ?>
                  <p class="text-white"><?php the_sub_field('sub_box4') ?></p>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
                <?php endwhile; ?>
  
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <!-- Section: Appuntamenti -->
  <section id="about">
   <div class="container pt-0">
    <div class="section-content">
     <div class="row">
      <div class="col-md-8">
        <div class="page-content">
          <?php if(get_field( 'doc_name' )): ?>
        <h2 class="text-theme-colored2 mb-0"><?php the_field('doc_name') ?></h2>
        <?php endif; ?>
        <?php if(get_field( 'doc_spec' )): ?>
        <p class="text-gray"><?php the_field('doc_spec') ?></p>
        <?php endif; ?>
        <?php if(get_field( 'title_app' )): ?>
        <h2 class="mt-0"><?php the_field('title_app') ?></h2>
        <?php endif; ?>
        <?php if(get_field( 'text_app' )): ?>
        <p class="lead"><?php the_field('text_app') ?></p>
        <?php endif; ?>
        </div>
      </div>
      <div class="col-md-4">
        <div class="p-30 pt-10 pb-20 border-theme-colored bg-theme-colored2">
          <?php if(get_field( 'title_form_app' )): ?>
          <h3 class="text-white"><?php the_field('title_form_app') ?></h3>
          <?php endif; ?>
          <?php if(get_field( 'sub_form_app' )): ?>
          <p class="text-white mt-0"><?php the_field('sub_form_app') ?></p>
          <?php endif; ?>
          <div id="appointment_form_at_home">
            <?php if(get_field( 'form_app' )): 
            echo do_shortcode(get_field('form_app'));
          endif ?>
         </div>
       </div>
       <div>
        
         <?php if (get_field('logo_conv')): ?>
           <?php
       $image = get_field('logo_conv');   
        $size = 'mobile-slider-thumb';
        $thumb = $image['sizes'][ $size ]; 
         $alt = $image['alt'];?>

         <div class="pt-20 logo-conv"><img src="<?php echo $thumb ?>" alt="<?php echo $alt; ?>"></div>
         <?php endif ?>
         
       </div>
     </div>
   </div>
 </div>
</div>
</section>



<!-- Section: Servizi -->
<section id="depertment" class="bg-silver-light">
  <div class="container">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <?php if(get_field( 'service_title' )): ?>
          <h2 class="text-uppercase mt-0 line-height-1"><?php the_field('service_title') ?></h2>
        <?php endif; ?>
          <div class="title-icon">
            <img class="mb-10" src="<?php bloginfo('template_directory')?>/assets/vendors/dentalpro/images/title-icon.png" alt="">
          </div>
          <?php if(get_field( 'service_sub' )): ?>
          <p><?php the_field('service_sub') ?></p>
          <?php endif ?>
        </div>
      </div>
    </div>
    <div class="section-content">
      <div class="owl-carousel-3col owl-carousel">
        <?php
          //ultime 5 news
        $args= array(
          'post_type' => 'servizi' ,
          'posts_per_page' => '-1',
          'orderby' => 'menu_order',
          'order'=> 'ASC'
        );
        $the_query = new WP_Query( $args );

            // Il Loop
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

         <div class="item">
          <div class="p-20 card-img">
            <?php 
            if ( '' != get_the_post_thumbnail() ) {
              the_post_thumbnail('blog_thumb');
            } else {
             ?><img src="<?php bloginfo('template_directory') ?>/assets/images/blog_thumb-placeholder.jpg');"> <?php 
           }
           ?>
           <h3><?php the_title(); ?></h3>
           <p class=""><?php echo excerpt(20); ?></p>
           <a href="<?php the_permalink() ?>" class="btn btn-flat btn-theme-colored mt-15 text-theme-color-2">Scopri di più</a>                
         </div>
       </div>              

      <?php endwhile;

            // Ripristina Query & Post Data originali
      wp_reset_query();
      wp_reset_postdata();
      ?>

    </div>
  </div>
</div>
</section>


<!-- start Doctors Section:-->
<section id="doctors">
  <div class="container">
    <div class="section-title text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <?php if(get_field( 'med_title' )): ?>
          <h2 class=" mt-0 line-height-1"><?php the_field('med_title') ?></h2>
          <?php endif ?>
          <div class="title-icon">
            <img class="mb-10" src="<?php bloginfo('template_directory')?>/assets/vendors/dentalpro/images/title-icon.png" alt="">
          </div>
          <?php if(get_field( 'med_sub' )): ?>
          <p><?php the_field('med_sub') ?></p>
          <?php endif ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 owl-carousel-4col owl-carousel">
        <?php
               
        $args= array(
          'post_type' => 'medici',
          'posts_per_page' => '-1',
          'orderby' => 'menu_order',
          'order'=> 'ASC'
        );
        $the_query = new WP_Query( $args );
                          // Il Loop
        while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

          <div class="item">
            <a href="<?php the_permalink(); ?>">
              <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
              <div class="team-thumb card-img">
                <?php 
                if ( '' != get_the_post_thumbnail() ) {
                  the_post_thumbnail('gal2x');
                } else {
                 ?><img src="<?php bloginfo('template_directory') ?>/assets/images/placeholder-400x400.jpg');"> <?php 
               }
               ?>
                <div class="team-overlay"></div>
              </div>
              <div class="team-details bg-silver-light med-card">
                <a href="<?php the_permalink(); ?>"> <h4 class="text-uppercase font-weight-600 m-5"><?php the_title(); ?></h4></a>
                <?php if(get_field( 'med_spec' )): ?>
                <h6 class="text-theme-colored font-15 font-weight-400 mt-0"><?php the_field('med_spec') ?></h6>
                <?php endif ?>
                <?php if( have_rows('social_med') ): ?>
                  <ul class="styled-icons icon-theme-colored icon-dark icon-circled icon-sm">
                    <?php while ( have_rows('social_med') ) : the_row(); ?>

                      <li><a href="<?php the_sub_field('link_social') ?>"><i class="fa fa-<?php the_sub_field('social') ?>"></i></a></li>

                      <?php endwhile; ?><!-- /while social_med-->
                    </ul>
                    <?php endif; ?><!--/if social_med-->
                  </div>
                </div>
            </a>
              </div>

            <?php endwhile;
                     // Ripristina Query & Post Data originali
            wp_reset_query();
            wp_reset_postdata(); ?>
          </div>
        </div>

      </div>
    </section>    <!-- /Doctors Section:-->


    <!--start testimonial Section-->
    <?php if(get_field( 'bg_trasparent_img' )){
          $image = get_field('bg_trasparent_img');   
          $size = 'slider-thumb';
          $thumb = $image['sizes'][ $size ];}
          else{
            $thumb = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
          }
     ?>
    <section class="divider parallax layer-overlay overlay-theme-colored-9" style="background-image:url('<?php echo $thumb ?>')">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <?php if(get_field('testimonial_title')): ?>
              <h2 class="text-uppercase text-white mt-0 line-height-1"><?php the_field('testimonial_title') ?></h2>
              <?php endif ?>
              <div class="title-icon">
                <img class="mb-10" src="<?php bloginfo('template_directory')?>/assets/vendors/dentalpro/images/title-icon-white.png" alt="">
              </div>
              <?php if(get_field('testimonial_sub')) :?>
              <p class="text-white"><?php the_field('testimonial_sub') ?></p>
              <?php endif ?>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel-3col owl-carousel" data-dots="true">

                <?php $args= array(
                    'post_type' => 'testimonianze',
                    'posts_per_page' => '-1'
                  );
                  $the_query = new WP_Query( $args );
                                    // Il Loop
                  while ( $the_query->have_posts() ) : $the_query->the_post(); ?>             
        
                    <div class="item">
                      <div class="testimonial style1">
                        <div class="comment bg-white">
                          <p class="lead text-black-333"><?php the_content() ?></p>
                        </div>
                        <div class="content mt-20">
                          <div class="thumb pull-right flip"> <?php the_post_thumbnail('gal4x', ['class' => 'img-circle']) ?> </div>
                          <div class="text-right flip pull-right flip mr-20 mt-10">
                            <h5 class="author text-theme-colored2"><?php the_title() ;?></h5>
                            <?php if (get_field('testimonial_type')): ?>
                              <h6 class="title text-white mt-0"><?php the_field('testimonial_type') ?></h6>
                            <?php endif ?>
                          </div>
                        </div>
                      </div>
                    </div> <!--/.item-->
                  <?php endwhile;
                     // Ripristina Query & Post Data originali
                      wp_reset_query();
                      wp_reset_postdata(); ?>
                </div><!--/.owl-carousel-->
            </div><!--/.col-->
          </div>
        </div> <!--/.section-content-->
      </div>
    </section> <!--/testimonial Section-->

    <!-- Section: Blog -->
    <section id="blog">
      <div class="container pb-50">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <?php if (get_field('news_title')): ?>
              <h2 class="mt-0 line-height-1"><?php the_field('news_title') ?></h2>
              <?php endif ?>
              <?php if (get_field('news_sub')): ?>
              <p><?php the_field('news_sub') ?></p>
              <?php endif ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 owl-carousel-3col owl-carousel" data-dots="true">
            <?php
          //ultime 5 news
            $args= array(
              'post_type' => 'post',
              'posts_per_page' => 3
            );
            $the_query = new WP_Query( $args );

            // Il Loop
            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

              <div class="item">
               <article class="clearfix maxwidth600 mb-30 wow fadeInRight" data-wow-delay=".2s">
                <div class="entry-header">
                  <div class="post-thumb thumb card-img"> 
                    <?php 
                        if ( '' != get_the_post_thumbnail() ) {
                          the_post_thumbnail('blog_thumb');
                        } else {
                         ?><img src="<?php bloginfo('template_directory') ?>/assets/images/blog_thumb-placeholder.jpg');"> <?php 
                       }
                      ?>
                  </div>
                </div>
                <div class="bg-theme-colored2 p-5 text-center pt-10 pb-10">
                  <span class="mb-10 text-white mr-10 font-13"><i class="fa fa-calendar mr-5 text-white"></i><?php the_date(); ?></span>
                </div>
                <div class="entry-content bg-lighter p-20 pr-10">
                  <div class="entry-meta mt-0 no-bg no-border">
                    <div class="event-content">
                      <h3 class="entry-title text-capitalize m-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                  </div>
                  <p class="mt-10"><?php echo excerpt(20); ?></p>
                  <div class="mt-10"> <a href="<?php the_permalink() ?>" class="btn btn-theme-colored btn-sm">Leggi di più</a></div>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
          <?php endwhile;

            // Ripristina Query & Post Data originali
          wp_reset_query();
          wp_reset_postdata();
          ?>
        </div>
        
      </div>
    </div>
  </section>

  <?php include('template-parts/bck-newsletter.php') ?>

<?php endwhile; // End of the loop. ?>

</div>
<!-- end main-content -->



<script type="text/javascript">
  jQuery(document).ready(function($){
    $(".owl-carousel-3col").owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      responsive:{
        0:{
          items:1,
          autoplay:true,
          autoplayTimeout:4000,
          autoplayHoverPause:true
        },
        600:{
          items:3,
          loop:false,
        }
      }
    })
    $(".owl-carousel-4col").owlCarousel({
      loop:true,
      nav:false,
      margin:20,
      responsive:{
        0:{
          items:1,
          autoplay:true,
          autoplayTimeout:4000,
          autoplayHoverPause:true
        },
        600:{
          items:4,
          loop:false,
        }
      }
    })
    $('.hero-slider').slick({
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 600,
       responsive: [
    {
      breakpoint: 576,
      settings: {
       arrows:false,
      }
    }],
    });
  });
</script>

<?php get_footer(); ?>
