<form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<input type="search" class="field-search" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php echo esc_attr_x( 'cerca &hellip;', 'placeholder', 'gleenk_unisalus' ); ?>" />
	<button type="submit" class="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
</form>
