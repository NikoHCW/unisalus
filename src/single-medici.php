<?php
/**
 * The template for Medici
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url('<?php bloginfo('template_directory') ?>/assets/images/doc.jpg')">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
										    <?php if(function_exists('bcn_display'))
										    {
										        bcn_display();
										    }?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>



					<!-- Section: Experts Details -->
					<section>
						<div class="container">
							<div class="section-content">
								<div class="row">
									<div class="col-md-4">
										<div class="thumb card-img">
											<?php 
											if(has_post_thumbnail()) {
												the_post_thumbnail('');
											} else {
												?><img src="<?php bloginfo('template_directory')?>/assets/images/placeholder-400x400.jpg"><?php
											}
											?>
										
										</div>
									</div>
									<div class="col-md-8">
										<h3 class="name font-24 mt-0 mb-0"><?php the_title() ?></h3>
										<?php if (get_field('med_spec')): ?>
											<h4 class="mt-5 text-theme-colored"><?php the_field('med_spec') ?></h4>
										<?php endif ?>
										<div class="page-content"><?php the_content(); ?></div>
										<?php if( have_rows('social_med') ): ?>
											<ul class="styled-icons icon-dark icon-theme-colored icon-sm mt-15 mb-0">
												<?php while ( have_rows('social_med') ) : the_row(); ?>
												<li><a href="<?php the_sub_field('link_social') ?>"><i class="fa fa-<?php the_sub_field('social') ?>"></i></a></li>
												<?php endwhile; ?><!-- /while social_med-->
											</ul>
										<?php endif; ?><!--/if social_med-->
									</div>
								</div>
								<div class="row mt-30">
									
								<?php if(get_field('med_exp') || get_field('med_address') || get_field('med_phone') || get_field( 'med_email' )): 
									//se non ho alcun campo, non mostro colonna e allargo l'altra ?>
									<div class="col-md-4 col-md-offset-4">
										<h4 class="line-bottom">Chi sono:</h4>
										<div class="volunteer-address">
											<ul>
												<?php if(get_field('med_exp')) : ?>
												<li>
													<div class="bg-light media border-bottom p-15 mb-20">
														<div class="media-left">
															<i class="pe-7s-pen text-theme-colored font-24 mt-5"></i>
														</div>
														<div class="media-body">
															<h5 class="mt-0 mb-0">Esperienze:</h5>
															<p><?php the_field('med_exp') ?></p>
														</div>
													</div>
												</li>
												<?php endif; ?>
												<?php /* if(get_field('med_address')) : ?>

												<li>
													<div class="bg-light media border-bottom p-15 mb-20">
														<div class="media-left">
															<i class="fa fa-map-marker text-theme-colored font-24 mt-5"></i>
														</div>
														<div class="media-body">
															<h5 class="mt-0 mb-0">Indirizzo:</h5>
															<?php if (get_field('med_address_link')){
																$link = get_field('med_address_link');
															}else{
																$link = '#';
															}									
															?>
															<p><a href="<?php echo $link ?>"><?php the_field('med_address') ?></a></p>
														</div>
													</div>											
												</li>
												<?php endif; */?>
												<?php if(get_field('med_phone') || get_field( 'med_email' )) : ?>
												<li>
													<div class="bg-light media border-bottom p-15">
														<div class="media-left">
															<i class="fa fa-phone text-theme-colored font-24 mt-5"></i>
														</div>
														<div class="media-body">
															<h5 class="mt-0 mb-0">Contatti:</h5>
															<p><span>Telefono:</span> <a href="tel:<?php the_field('med_phone'); ?>"><?php the_field('med_phone'); ?></a><br><span>Email:</span> <a href="mailto:<?php print antispambot(get_field('med_email')); ?>"><?php print antispambot (get_field('med_email')); ?></a></p>
														</div>
													</div>
												</li>
												<?php endif; ?>
											</ul>
										</div>
									</div>
								<?php endif; //fine check presenza campi ?>
								
								<?php 
								if(get_field('med_exp') || get_field('med_address') || get_field('med_phone') || get_field( 'med_email' )): 
									$col_class = 'col-md-4'; //se ho almeno un campo del box precedente mantiene col-md-4
								else:
									$col_class = 'col-md-8 col-md-offset-4'; //se no allargo
								endif;
								?>
								<div class="<?php echo $col_class; ?>">
									<div class="clearfix">
										<h4 class="line-bottom"><?php _e('Contatta Unisalus','unisalus') ?>:</h4>
									</div>
									
									<div id="contact-form" class="contact-form-transparent main-form">
										<?php echo do_shortcode('[contact-form-7 id="368" title="Medico"]') ?>
									</div>
								
									
								</div>
							</div>
						</div>
					</div>
				</section>

				
			</div>
			<!-- end main-content -->
		</article>

	<?php endwhile; // End of the loop. ?>

</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
