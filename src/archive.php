<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package starter
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url('<?php bloginfo('template_directory') ?>/assets/images/doc.jpg'); ?>)">
				<div class="container flex-c ">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-md-12">
								<h1 class="title text-center"><?php the_archive_title(); ?></h1>
								<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
								<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
									<?php if(function_exists('bcn_display'))
									{
										bcn_display();
									}?>
								</div><!--/.breadcrumbs-->
							</div>
						</div>
					</div>
				</div>
			</header>

			<?php /* Start the Loop */ ?>
			<section>
			<div class="container mt-30 mb-30 pt-30 pb-30">
			<div class="row multi-row-clearfix">
			<div class="blog-posts">
			<div class="col-md-12">
			<div class="list-dashed">
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('post clearfix mb-30 pb-30'); ?>>
					<div class="row">
						<div class="col-sm-5">
							<div class="entry-header">
								<div>
									<?php if (get_the_post_thumbnail()){
										the_post_thumbnail('blog_thumb');
									}else{
										?><img src="<?php bloginfo('template_directory') ?>/assets/images/blog_thumb-placeholder.jpg');"> <?php
									}

									?>
								</div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="entry-content mt-0">
								<a href="<?php the_permalink(); ?>">
									<h4 class="entry-title mt-0 pt-0"><?php the_title() ?></h4>
								</a>
								<ul class="list-inline font-12 mb-20 mt-10">
									<span class="text-theme-colored"><?php the_date() ?></span></li>
								</ul>
								<p class="mb-30"><?php echo excerpt(20); ?></p>
								<ul class="list-inline like-comment pull-left font-12">
									<li><i class="pe-7s-comment"></i><?php comments_number( '0 Commenti', '1 Commento', '% Commenti' ); ?></li>

								</ul>
								<a class="pull-right text-gray font-13" href="<?php the_permalink(); ?>"><i class="fa fa-angle-double-right text-theme-colored"></i>Leggi di più</a>
							</div>
						</div>
					</div>
				</article>
			<?php endwhile; ?>
			</div>
			</div>
			</div>
			</div><!--/.row-->
			</div><!--/.container-->
			</section>


			<?php wp_pagenavi(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
