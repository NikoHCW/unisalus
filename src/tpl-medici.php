<?php
/**
 * Template Name: TPL Medici
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<?php 
					if(get_the_post_thumbnail() == ""){
							//$bg = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
						$bg = get_bloginfo('template_directory') . '/assets/images/doc.jpg';
					} else {
						$bg = get_the_post_thumbnail_url(get_the_ID(),'full') ;
					}
					?>
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php echo $bg; ?>)">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php if (get_the_content()): ?>
						<section>
							<div class="container">			
								<?php the_content(); ?>
							</div>
						</section>
					<?php endif ?>
					

					<section id="doctors">
						<div class="container">
							<div class="row">
								<?php
         						 //ultime 5 news
								$args= array(
									'post_type' => 'medici',
									'posts_per_page' => '-1',
									'orderby' => 'menu_order',
									'order'=> 'ASC'
								);
								$the_query = new WP_Query( $args );
          							  // Il Loop
								while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="<?php the_permalink(); ?>">
											<div class="team-members border-bottom-theme-color-2px text-center maxwidth400 med-page">
												<div class="team-thumb card-img">
													<?php 
													if(has_post_thumbnail()) {
														the_post_thumbnail('gal2x');
													} else {
														?><img src="<?php bloginfo('template_directory')?>/assets/images/placeholder-400x400.jpg"><?php
													}
													?>

													<div class="team-overlay"></div>
												</div>
												<div class="team-details bg-silver-light pt-10 pb-10 med-card">
													<a href="<?php the_permalink(); ?>"><h4 class="text-uppercase font-weight-600 m-5"><?php the_title(); ?></h4></a>
													<?php if (get_field('med_spec') ): ?>
														<h6 class="text-theme-colored font-15 font-weight-400 mt-0"><?php the_field('med_spec') ?></h6>
													<?php endif ?>
													<?php if( have_rows('social_med') ): ?>
														<ul class="styled-icons icon-circled icon-sm med_-social">
															<?php while ( have_rows('social_med') ) : the_row(); ?>

																<li><a href="<?php the_sub_field('link_social') ?>"><i class="fa fa-<?php the_sub_field('social') ?>"></i></a></li>

																<?php endwhile; ?><!-- /while social_med-->
															</ul>
															<?php endif; ?><!--/if social_med-->
														</div>
													</div>
												</div>
											</a>

										<?php endwhile;
           					 // Ripristina Query & Post Data originali
										wp_reset_query();
										wp_reset_postdata(); ?>
									</div>
								</div>
							</section>    <!-- /Doctors Section:-->



						</div>
						<!-- end main-content -->
					</article>

				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->


		<?php get_footer(); ?>
