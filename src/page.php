<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package starter
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main page-content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php 
			if(get_the_post_thumbnail() == ""){
				//$bg = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
				$bgcolor = "#eee";
			} else {
				$bg = get_the_post_thumbnail_url(get_the_ID(),'full') ;
			}
			?>
			<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php echo $bg;?>); background-color:<?php echo $bgcolor; ?>;">
				<div class="container flex-c">
					<!-- Section Content -->
					<div class="section-content">
						<div class="row">
							<div class="col-md-12">
								<h2 class="title text-center"><?php the_title(); ?></h2>
								<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
									<?php if(function_exists('bcn_display'))
									{
										bcn_display();
									}?>
								</div><!--/.breadcrumbs-->
							</div>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">			
				<?php the_content(); ?>
				</div>
			</section>

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
