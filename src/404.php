<?php
/**
* The template for displaying 404 pages (not found).
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package starter
*/

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		
		<div class="main-content">     
			<!-- Section: home -->
			<section id="home" class="fullscreen bg-lightest">
				<div class="display-table text-center">
					<div class="display-table-cell">
						<div class="container pt-0 pb-0">
							<div class="row">
								<div class="col-md-12">
									<h1 class="font-100 line-height-1em mt-0 mb-0 text-theme-colored">404!</h1>
									<h2 class="mt-0"><?php esc_html_e( 'Oops! Nessuna pagina trovata.', 'unisalus' ); ?></h2>
									<a class="btn btn-border btn-gray btn-transparent btn-circled smooth-scroll-to-target" href="<?php echo get_home_url(); ?>">Torna alla Home
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> 
		</div>  
		<!-- end main-content -->


	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
