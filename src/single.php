<?php


get_header(); ?>

<div id="primary" class="content-area post">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<?php 
						if(get_the_post_thumbnail() == ""){
							//$bg = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
							$bgcolor = "#eee";
						} else {
							$bg = get_the_post_thumbnail_url(get_the_ID(),'full') ;
						}
					 ?>
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php echo $bg; ?>); background-color: <?php echo $bgcolor; ?>">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="container mt-30 mb-30 pt-30 pb-30">
							<div class="row">
								<div class="col-md-9">
									<div class="blog-posts single-post">
										<article class="blog-article clearfix mb-0">
											<div class="entry-content">
												<div class="entry-meta media no-bg no-border mt-15 pb-20">
													<div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
														<ul>
															<li class="font-12 text-white text-uppercase"><?php the_date(); ?></li>
														</ul>
													</div>
													<div class="media-body pl-15">
														<div class="event-content pull-left flip">
															<h3 class="entry-title text-white text-uppercase pt-0 mt-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
														</div>
													</div>
												</div>
												<div class="page-content"><?php the_content(); ?></div>

											</div>
										</article>
										<div class="tagline p-0 pt-20 mt-5">
											<div class="row">
												<div class="col-md-8">
													<div class="tags">
														<p class="mb-0"><i class="fa fa-tags text-theme-colored"></i> <?php the_tags() ?></p>
													</div>
												</div>
												
											</div>
										</div>
									

									</div>
								</div>
								<div class="col-md-3">
									<div class="sidebar sidebar-left mt-sm-30">
										
										<div class="widget">
											<h4 class="widget-title">Le ultime News</h4>
											<div class="latest-posts">
												 <?php
          											//ultime 6 news
										            $args= array(
										              'post_type' => 'post',
										              'posts_per_page' => 6,
										              //Nascondere il post in cui sono dalla lista 'post__not_in' => array(get_the_ID())
										            );
										            $the_query = new WP_Query( $args );

										            // Il Loop
										            while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

												<article class="post media-post clearfix pb-0 mb-10 flex-ai">

													<a href="<?php the_permalink(); ?>" class="post-thumb">
														<?php if (get_the_post_thumbnail()){
															the_post_thumbnail('gal4x');
														}else{
															?><img src="<?php bloginfo('template_directory') ?>/assets/images/placeholder-100x100.jpg');"> <?php
														}

														?>
														</a>
													<div class="post-right">
														<h5 class="post-title mt-0 mb-0"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
														<p class="post-date font-12"><?php echo get_the_date() ?></p>
														
													</div>
												</article>
												<?php endwhile;

										            // Ripristina Query & Post Data originali
										          wp_reset_query();
										          wp_reset_postdata();
										          ?>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section> 



				</div>
				<!-- end main-content -->
			</article>

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
