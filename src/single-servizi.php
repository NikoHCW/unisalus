<?php
/**
 * The template for Servizi
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->

					<?php 
					if(get_the_post_thumbnail() == ""){
							//$bg = get_bloginfo('template_directory') . '/assets/images/placeholder-1920x1080.jpg';
						$bgcolor = "#eee";
					} else {
						$bg = get_the_post_thumbnail_url(get_the_ID(),'full') ;
					}
					?>
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php echo $bg;?>); background-color: <?php echo $bgcolor; ?>">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>

					<!-- Section: Services -->
					<section>
						<div class="container">
							<div class="row mtli-row-clearfix">
								<div class="col-sm-6 col-md-8 col-lg-8">
									<div class="event-details page-content">
										<?php the_content(); ?>
									</div>
								</div>
								<div class="col-sm-6 col-md-4 col-lg-4">
									<div class="sidebar sidebar-right mt-sm-30">
										<div class="widget">
											<h5 class="widget-title line-bottom">Tutti i Servizi</h5>
											<?php $id_pagina = get_the_ID() ?>

											<?php 
											$args = array(
												'post_type' => 'servizi',
												'posts_per_page' => '-1',
	      									//'post__not_in' => array(get_the_ID())

											);
											$the_query = new WP_Query( $args );
											while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

												<?php if ($id_pagina == get_the_ID()){ 
													$current = 'aria-current="page"';
												}else{
													$current = '';
												} 
												?>

												<div class="list-border py-10">
													<a <?php echo $current; ?> href="<?php the_permalink() ?>"><?php the_title() ?></a>
												</div>


											<?php endwhile;
           					 // Ripristina Query & Post Data originali
											wp_reset_query();
											wp_reset_postdata(); ?>

										</div>
										<div class="widget">
											<div class="border-10px p-30">
												<h5><i class="fa fa-clock-o text-theme-colored"></i> Orari d'apertura</h5>
												<div class="opening-hours text-left">
													<ul class="list-border">
														<?php if( have_rows('orario', 'option') ): 
															while ( have_rows('orario', 'option') ) : the_row(); ?>

																<?php if(get_sub_field('days','option')) :?>
																	<li class="clearfix"> <span> <?php the_sub_field('days', 'option'); ?> :  </span>
																	<?php endif ?>
																	<div class="value pull-right flip"> <?php if(get_sub_field('time_open','option')) : the_sub_field('time_open', 'option'); endif?> - <?php 
																	if(get_sub_field('time_close', 'option')) : the_sub_field('time_close', 'option'); endif?> </div>
																</li>

															<?php endwhile;
														endif;
														if (get_field('day_close', 'option')) : ?>
															<li class="clearfix"> <span> <?php the_field('day_close', 'option'); ?> : </span>
															<?php endif ?>
															<div class="value pull-right flip"> Chiuso </div>
														</li>
													</ul>
												</div>
												<?php if (get_field('contact_url','option') && get_field('contact_label','option')): ?>
												

												<a href="<?php the_field('contact_url', 'option'); ?>" class="btn btn-dark btn-theme-colored mt-15"><?php the_field('contact_label', 'option'); ?></a>
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php /*
						<div class="row">
							<div class="col-md-12">
								<h3 class="title mb-30 line-bottom">Prezzi</h3>
							</div>
							<?php
						// check if the repeater field has rows of data
						if( have_rows('tab_price') ):

						 	// loop through the rows of data
						    while ( have_rows('tab_price') ) : the_row();?>
							<div class="col-xs-12 col-sm-6 col-md-6 pb-sm-20 list-border no-padding">	
								<h5><?php the_sub_field('service_name') ?> <span class="pull-right flip font-weight-400 pr-20"><?php the_sub_field('service_price') ?></span></h5>
								<p><?php the_sub_field('service_desc') ?></p>				
							</div>
						<?php endwhile; endif; ?>
						</div>
					    */?>
					</div>
				</section>

				<?php include('template-parts/bck-newsletter.php') ?>


				<!-- Divider: Funfact -->
				
			</div>
			<!-- end main-content -->
		</article>

	<?php endwhile; // End of the loop. ?>

</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
