<?php
/**
 * Template Name: TPL Promozioni
 */
get_header(); ?>

<div id="primary" class="content-area blog">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- Start main-content -->
				<div class="main-content">
					<!-- Section: inner-header -->
					<section class="inner-header divider parallax layer-overlay overlay-white-2" style="background-image:url(<?php the_post_thumbnail_url('full'); ?>)">
						<div class="container flex-c">
							<!-- Section Content -->
							<div class="section-content">
								<div class="row">
									<div class="col-md-12">
										<h2 class="title text-center"><?php the_title(); ?></h2>
										<div class="breadcrumbs text-center mt-10" typeof="BreadcrumbList" vocab="https://schema.org/">
											<?php if(function_exists('bcn_display'))
											{
												bcn_display();
											}?>
										</div><!--/.breadcrumbs-->
									</div>
								</div>
							</div>
						</div>
					</section>
					

					<section>
					
						<div class="container mt-20 mb-30 pt-10 pb-30 grey-bg">
								<?php
									// check if the repeater field has rows of data
									if( have_rows('promo_type') ):
										while ( have_rows('promo_type') ) : the_row();?>

											<div class="mb-40">
												<?php if (get_sub_field('h2_promo')): ?>
													<h2 class="text-center"><?php the_sub_field('h2_promo') ?></h2>
												<?php endif ?>
												<?php if (get_sub_field('desc_promo')): ?>
													<p class="text-center"><?php the_sub_field('desc_promo') ?></p>
												<?php endif ?>
											
											</div>
							<div class="row multi-row-clearfix">
								<div class="blog-posts">
									
											<?php if( have_rows('promo') ):
										while ( have_rows('promo') ) : the_row();

											 $image = get_sub_field('logo_promo');   
									          $size = 'blog_thumb';
									          $thumb = $image['sizes'][ $size ]; 
									           $alt = $image['alt'];?>
									          


											<div class="col-md-12">
												<div class="list-dashed">
													<div class="post clearfix mb-30 pb-30">
														<div class="row flex-c">
															<div class="col-sm-5">
																<div class="entry-header">
																	<div><img src="<?php echo $thumb ?>" alt="<?php echo $alt; ?>"></div>
																</div>
															</div>
															<div class="col-sm-7">
																<div class="entry-content mt-0">
																	<a href="<?php the_sub_field('link_promo') ?>">
																		<h4 class="entry-title mt-0 pt-0"><?php the_sub_field('name_promo') ?></h4>
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										<?php endwhile;	endif;?>
								</div>
							</div>

						<?php endwhile;	endif;?>
						</div>
					</section>



		</div>
		<!-- end main-content -->
	</article>

<?php endwhile; // End of the loop. ?>

</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer(); ?>
