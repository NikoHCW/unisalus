<?php

require_once('includes/custom-post-type.php');
//require_once('includes/custom-widgets.php');
require get_template_directory() . '/includes/template-tags.php';

if ( ! function_exists( 'unisalus_setup' ) ) :

/* IMPOSTAZIONI DI BASE */
function unisalus_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-logo' );
	
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'unisalus' ),
		'servizi' => __( 'Menu Servizi', 'unisalus' ),
	) );
}
endif; // unisalus_setup
add_action( 'after_setup_theme', 'unisalus_setup' );



/* PRESET PERSONALIZZATI */

add_image_size( 'slider-thumb', 1440, 640, true );
add_image_size( 'mobile-slider-thumb', 720, 380, true );

add_image_size( 'gal2x', 450, 450, true);
add_image_size( 'gal3x', 300, 300, true);
add_image_size( 'gal4x', 250, 250, true);

add_image_size( 'blog_thumb', 460, 240, true);

//* Add new image sizes to post or page editor
add_filter( 'image_size_names_choose', 'mytheme_image_sizes' );
function mytheme_image_sizes( $sizes ) {

    $mythemesizes = array(
        'gal2x' 		=> __( 'Square Gallery 2 Photos' ), 
        'gal3x' 		=> __( 'Square Gallery 3 Photos' ), 
        'gal4x' 		=> __( 'Square Gallery 4 Photos' ), 
    );
    $sizes = array_merge( $sizes, $mythemesizes );

    return $sizes;
}

/* CREAZIONE SIDEBAR */
function unisalus_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'unisalus' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'unisalus' ),
		'id'            => 'footer-1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'unisalus_widgets_init' );


function unisalus_scripts() {
	if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
	function my_jquery_enqueue() {

	   wp_enqueue_script( 'jquery' );
	   wp_enqueue_script( 'html5shiv','https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js', array(), '', true);
	   //JS DENTALPRO
	   wp_enqueue_script( 'bootstrap.min', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/js/bootstrap.min.js', array(), '', true);

	   wp_enqueue_script( 'owlcarousel', get_template_directory_uri() . '/assets/vendors/owlcarousel/owl.carousel.min.js', array(), '2.3.4', true );

	   wp_enqueue_script( 'counter-up', get_template_directory_uri() . '/assets/vendors/Counter-Up-master/jquery.counterup.min.js', array(), '', true );
	   //JQUERY CUSTOM IN CODA
	   wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/scripts.js', array(), '1.0.0', true );
	   //SLICK js
	   wp_enqueue_script( 'slick','//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '', true);
	}
}
add_action( 'wp_enqueue_scripts', 'unisalus_scripts' );

function unisalus_styles() {
    wp_enqueue_style( 'meyer', 'https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css' );
    //CSS DENTALPRO
    wp_enqueue_style( 'animate-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/animate.css' );
    wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/bootstrap.min.css' );
   
    wp_enqueue_style( 'css-plugin-collections-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/css-plugin-collections.css' );
    wp_enqueue_style( 'custom-bootstrap-margin-padding-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/custom-bootstrap-margin-padding.css' );
    wp_enqueue_style( 'elegant-icons-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/elegant-icons.css' );
    wp_enqueue_style( 'flaticon-set-dental-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/flaticon-set-dental.css' );
    wp_enqueue_style( 'flaticon-set-medical-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/flaticon-set-medical.css' );
    wp_enqueue_style( 'font-awesome-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/font-awesome.min.css' );
    wp_enqueue_style( 'font-awesome-animation-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/font-awesome-animation.min.css' );
    wp_enqueue_style( 'icomoon-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/icomoon.css' );
    wp_enqueue_style( 'ionicons-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/ionicons.css' );
    wp_enqueue_style( 'jquery-ui-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/jquery-ui.min.css' );
    //wp_enqueue_style( 'medinova-font-icons-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/medinova-font-icons.css' );
    //wp_enqueue_style( 'menuzord-megamenu-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/menuzord-megamenu.css' );
    
    wp_enqueue_style( 'pe-icon-7-stroke-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/pe-icon-7-stroke.css' );
    wp_enqueue_style( 'preloader-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/preloader.css' );
    wp_enqueue_style( 'responsive-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/responsive.css' );
    wp_enqueue_style( 'stroke-gap-icons-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/stroke-gap-icons.css' );
   
   
    wp_enqueue_style( 'utility-classes-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/utility-classes.css' );
    //skin color

   /* wp_enqueue_style( 'theme-skin-color-set1-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/colors/theme-skin-color-set1.css' );
    wp_enqueue_style( 'theme-skin-color-set2-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/colors/theme-skin-color-set2.css' );
    
    wp_enqueue_style( 'theme-skin-color-set4-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/colors/theme-skin-color-set4.css' );
    wp_enqueue_style( 'theme-skin-color-set5-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/colors/theme-skin-color-set5.css' );*/
    wp_enqueue_style( 'theme-skin-color-set3-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/colors/theme-skin-color-set3.css' );

    wp_enqueue_style( 'style-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/style.css' );
    wp_enqueue_style( 'style-main-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/style-main.css' );
    wp_enqueue_style( 'style-main-dark-css', get_stylesheet_directory_uri() . '/assets/vendors/dentalpro/css/style-main-dark.css' );

    wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css' );
    //MAIN CSS
    wp_enqueue_style( 'main-css', get_stylesheet_directory_uri() . '/assets/css/main.css' );

    //CSS CUSTOM LIBERO PER CLIENTE
    wp_enqueue_style( 'unisalus_style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'unisalus_styles' ); 

/* EXCERPT CON LUNGHEZZA PERSONALIZZATA - USARE NEL TEMA:*/

/* EXCERPT CON LUNGHEZZA PERSONALIZZATA - USARE NEL TEMA:*/
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

// CUSTOM ADMIN LOGO e BACKGROUND
add_action('login_head', 'my_custom_login_logo');
function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/assets/images/logo-admin.jpg) !important; background-size:auto !important; width: 242px !important; height:51px !important; }
		body.login { background: url('.get_bloginfo('template_directory').'/assets/images/background-admin.png) !important;}
    </style>';
}
 
// CUSTOM ADMIN LOGO URL
add_filter( 'login_headerurl', 'cwwp_login_logo_url' );
function cwwp_login_logo_url( $url ) {
	return trailingslashit( get_home_url() );
}

// CUSTOM ADMIN CSS
add_action('admin_head', 'my_custom_admin_css');
function my_custom_admin_css() {
echo '<style>
#wp-admin-bar-perseoweb .ab-item,
#wp-admin-bar-perseoweb:hover .ab-item { background:url('.get_bloginfo('template_directory').'/assets/images/perseoweb.png) no-repeat !important; width:133px; text-indent:-9999px; }
</style>';
}


function webriti_toolbar_link($wp_admin_bar) {
    $args = array(
        'id' => 'perseoweb',
        'title' => 'Perseoweb',
        'href' => 'https://www.perseoweb.it/',
        'meta' => array(
            'class' => 'customlink',
            'title' => 'Contatta Perseoweb'
            )
        );
    $wp_admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'webriti_toolbar_link', 999);

// PREVENT PAGE DELETION
function restrict_post_deletion($post_ID){
    $user = get_current_user_id();
    //$restricted_users = array(2);
    $restricted_pages = array(2);
    if(in_array($post_ID, $restricted_pages)){
        echo "Attenzione! Non è possibile rimuovere la seguente pagina causa la compromissione delle funzionalità dell'intero sito.";
        exit;
    }
}
add_action('wp_trash_post', 'restrict_post_deletion', 10, 1);
add_action('before_delete_post', 'restrict_post_deletion', 10, 1);

//disattiva pallini YOAST
add_filter( 'wpseo_use_page_analysis', '__return_false' );
//sposta YOAST sotto
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


/* hide ACF MENU */
//add_filter('acf/settings/show_admin', '__return_false');

if(function_exists('acf_add_options_page')) { 
	acf_add_options_page();
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
	acf_add_options_sub_page('Tabella Orari');
	acf_add_options_sub_page('Contatti');
	acf_add_options_sub_page('Google Analytics');
}

// RIMUOVI WELCOME PANEL
remove_action( 'welcome_panel', 'wp_welcome_panel' ); 

