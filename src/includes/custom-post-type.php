<?php
/***********************************************/
/* = POST TYPE "SERVIZI" */
/***********************************************/
function post_type_servizi() {

    $labels = array(
        'name'                  => _x( 'Servizi', 'Post Type General Name', 'unisalus' ),
        'singular_name'         => _x( 'Servizio', 'Post Type Singular Name', 'unisalus' ),
        'menu_name'             => __( 'Servizio', 'unisalus' ),
        'name_admin_bar'        => __( 'Servizi', 'unisalus' ),
        'archives'              => __( 'Archivio Servizi', 'unisalus' ),
        'parent_item_colon'     => __( 'Parent Servizi:', 'unisalus' ),
        'all_items'             => __( 'Tutte le Servizi', 'unisalus' ),
        'add_new_item'          => __( 'Aggiungi Nuovo Servizio', 'unisalus' ),
        'add_new'               => __( 'Aggiungi Nuovo', 'unisalus' ),
        'new_item'              => __( 'Nuovo Servizio', 'unisalus' ),
        'edit_item'             => __( 'Modifica Servizio', 'unisalus' ),
        'update_item'           => __( 'Aggiorna Servizio', 'unisalus' ),
        'view_item'             => __( 'Vedi Servizio', 'unisalus' ),
        'search_items'          => __( 'Cerca Servizio', 'unisalus' ),
        'not_found'             => __( 'Non Trovato', 'unisalus' ),
        'not_found_in_trash'    => __( 'Non Trovato nel Cestino', 'unisalus' ),
        'featured_image'        => __( 'Immagine in Evidenza', 'unisalus' ),
        'set_featured_image'    => __( 'Imposta Immagine in Evidenza', 'unisalus' ),
        'remove_featured_image' => __( 'Rimuovi Immagine in Evidenza', 'unisalus' ),
        'use_featured_image'    => __( 'Use as featured image', 'unisalus' ),
        'insert_into_item'      => __( 'Insert into item', 'unisalus' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'unisalus' ),
        'items_list'            => __( 'Items list', 'unisalus' ),
        'items_list_navigation' => __( 'Items list navigation', 'unisalus' ),
        'filter_items_list'     => __( 'Filter items list', 'unisalus' ),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'unisalus' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'servizi', 'with_front' => false ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-building',
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','page-attributes' )
    );
    register_post_type( 'servizi', $args );

}
add_action( 'init', 'post_type_servizi', 0 );

/***********************************************/
/* = POST TYPE "MEDICI " */
/***********************************************/
function post_type_medici() {

    $labels = array(
        'name'                  => _x( 'Medici', 'Post Type General Name', 'unisalus' ),
        'singular_name'         => _x( 'Medico', 'Post Type Singular Name', 'unisalus' ),
        'menu_name'             => __( 'Medico', 'unisalus' ),
        'name_admin_bar'        => __( 'Medici', 'unisalus' ),
        'archives'              => __( 'Archivio Medici', 'unisalus' ),
        'parent_item_colon'     => __( 'Parent Medici:', 'unisalus' ),
        'all_items'             => __( 'Tutte le Medici', 'unisalus' ),
        'add_new_item'          => __( 'Aggiungi Nuovo Medico', 'unisalus' ),
        'add_new'               => __( 'Aggiungi Nuovo', 'unisalus' ),
        'new_item'              => __( 'Nuovo Medico', 'unisalus' ),
        'edit_item'             => __( 'Modifica Medico', 'unisalus' ),
        'update_item'           => __( 'Aggiorna Medico', 'unisalus' ),
        'view_item'             => __( 'Vedi Medico', 'unisalus' ),
        'search_items'          => __( 'Cerca Medico', 'unisalus' ),
        'not_found'             => __( 'Non Trovato', 'unisalus' ),
        'not_found_in_trash'    => __( 'Non Trovato nel Cestino', 'unisalus' ),
        'featured_image'        => __( 'Immagine in Evidenza', 'unisalus' ),
        'set_featured_image'    => __( 'Imposta Immagine in Evidenza', 'unisalus' ),
        'remove_featured_image' => __( 'Rimuovi Immagine in Evidenza', 'unisalus' ),
        'use_featured_image'    => __( 'Use as featured image', 'unisalus' ),
        'insert_into_item'      => __( 'Insert into item', 'unisalus' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'unisalus' ),
        'items_list'            => __( 'Items list', 'unisalus' ),
        'items_list_navigation' => __( 'Items list navigation', 'unisalus' ),
        'filter_items_list'     => __( 'Filter items list', 'unisalus' ),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'unisalus' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'medici', 'with_front' => false ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-universal-access-alt',
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes' )
    );
    register_post_type( 'medici', $args );

}
add_action( 'init', 'post_type_medici', 0 );

/***********************************************/
/* = POST TYPE "TESTIMONIANZE" */
/***********************************************/
function post_type_testimonianze() {

    $labels = array(
        'name'                  => _x( 'Testimonianze', 'Post Type General Name', 'unisalus' ),
        'singular_name'         => _x( 'Testimonianza', 'Post Type Singular Name', 'unisalus' ),
        'menu_name'             => __( 'Testimonianza', 'unisalus' ),
        'name_admin_bar'        => __( 'Testimonianze', 'unisalus' ),
        'archives'              => __( 'Archivio Testimonianze', 'unisalus' ),
        'parent_item_colon'     => __( 'Parent Testimonianze:', 'unisalus' ),
        'all_items'             => __( 'Tutte le Testimonianze', 'unisalus' ),
        'add_new_item'          => __( 'Aggiungi Nuovo Testimonianza', 'unisalus' ),
        'add_new'               => __( 'Aggiungi Nuovo', 'unisalus' ),
        'new_item'              => __( 'Nuovo Testimonianza', 'unisalus' ),
        'edit_item'             => __( 'Modifica Testimonianza', 'unisalus' ),
        'update_item'           => __( 'Aggiorna Testimonianza', 'unisalus' ),
        'view_item'             => __( 'Vedi Testimonianza', 'unisalus' ),
        'search_items'          => __( 'Cerca Testimonianza', 'unisalus' ),
        'not_found'             => __( 'Non Trovato', 'unisalus' ),
        'not_found_in_trash'    => __( 'Non Trovato nel Cestino', 'unisalus' ),
        'featured_image'        => __( 'Immagine in Evidenza', 'unisalus' ),
        'set_featured_image'    => __( 'Imposta Immagine in Evidenza', 'unisalus' ),
        'remove_featured_image' => __( 'Rimuovi Immagine in Evidenza', 'unisalus' ),
        'use_featured_image'    => __( 'Use as featured image', 'unisalus' ),
        'insert_into_item'      => __( 'Insert into item', 'unisalus' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'unisalus' ),
        'items_list'            => __( 'Items list', 'unisalus' ),
        'items_list_navigation' => __( 'Items list navigation', 'unisalus' ),
        'filter_items_list'     => __( 'Filter items list', 'unisalus' ),
    );
    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'unisalus' ),
        'public'             => true,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'testimonianze', 'with_front' => false ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-star-filled',
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );
    register_post_type( 'testimonianze', $args );

}
add_action( 'init', 'post_type_testimonianze', 0 );
