<?php 

/*
// Register and load the widget
function widget_social_loag() {
    register_widget( 'social_widget' );
}
add_action( 'widgets_init', 'widget_social_loag' );

// Creating the widget 
class social_widget extends WP_Widget {

function __construct() {
parent::__construct(

// Base ID of your widget
'social_widget', 

// Widget name will appear in UI
__('Social Widget', 'social_widget_domain'), 

// Widget description
array( 'description' => __( 'Social Widget', 'social_widget_domain' ), ) 
);
}

// Creating widget front-end

public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
$widget_id = $args['widget_id']; //important!!
?>
    <?php if( have_rows('social_list_widget', 'widget_' . $widget_id) ): ?>
            <ul class="social-list">
            <?php while ( have_rows('social_list_widget', 'widget_' . $widget_id) ) : the_row(); ?>

                <li><a href="<?php the_sub_field('url_social', 'widget_' . $widget_id); ?>" style="background-color: <?php the_sub_field('colore_sfondo_social', 'widget_' . $widget_id); ?>"><?php the_sub_field('icona_social', 'widget_' . $widget_id); ?></a></li>
                

            <?php endwhile; ?>
            </ul>
        <?php endif;?>
<?php
// This is where you run the code and display the output
echo $args['after_widget'];
}
        
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'social_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
    
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class social_widget ends here

*/