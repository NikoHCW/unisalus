<!-- Newsletter -->
<section class="area-newsletter-form bg-theme-colored">
	<div class="container">
		<div class="row flex-c">
			<div class="col-md-6">
				<h3 class="text-uppercase text-white mt-0 line-height-1">Iscriviti alla</h3>
				<h2 class="fs-40 text-white mt-0 line-height-1">Newsletter</h2>
			</div>
			<?php if (get_field('form_news',2)): ?>
				<div class="col-md-6 newsletter-home">

					<?php echo do_shortcode(get_field('form_news',2)) ?>

				</div>
			<?php endif ?>
		</div>
	</div>
</section>
