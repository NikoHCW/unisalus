<!DOCTYPE html>
<!--[if IE 8 ]><html dir="ltr" lang="it-IT" class="no-js ie8 oldie"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" /><!-- valutare viewport-fit=cover per iPhoneX con https://css-tricks.com/designing-websites-iphone-x/ -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title><?php wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>
<?php the_field('analytics_code','option') ?>
</head>

<body class="has-side-panel side-panel-right fullwidth-page">
<div id="wptime-plugin-preloader"></div>
<div id="wrapper">

  
  <!-- Header -->
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-light sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="widget">
              <i class="fa fa-clock-o text-theme-colored"></i> Orari: <?php if( have_rows('orario', 'option') ): while ( have_rows('orario', 'option') ) : the_row(); the_sub_field('days', 'option');?> <?php the_sub_field('time_open', 'option'); ?> - <?php the_sub_field('time_close', 'option'); endwhile; endif; ?> / <?php the_field('day_close', 'option'); ?> : Chiuso

            </div>
          </div>
          <div class="col-md-6">
            <div class="widget">
              <ul class="list-inline pull-right flip sm-pull-none sm-text-center">
                <li><i class="fa fa-phone text-theme-colored"></i> Chiamaci al <a href="tel:<?php the_field('telefono', 'option'); ?>"><?php the_field('telefono', 'option'); ?></a></li>
                <li><i class="fa fa-envelope-o text-theme-colored"></i> <a href="mailto:<?php print antispambot(the_field('email', 'option')) ?>"><?php print antispambot(the_field('email', 'option')) ?></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
        <div class="container flex-sb">
           <div class="site-branding">
              <?php if ( is_front_page() ) : ?>
              <h1 class="site-title"><?php if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); }; ?></h1>
              <?php else : ?>
               <div class="site-title"><?php if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); }; ?></div>
              <?php endif; ?>
           </div>

          <nav id="site-navigation" class="blue no-bg site-navigation main-navigation" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
          </nav>
          <div id="pull"><i class="fa fa-bars"></i></div>
        </div>
      </div>
    </div>
  </header>

<?php /*
<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><?php if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); }; ?></h1>
			<?php else : ?>
				<div class="site-title"><?php if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); }; ?></div>
			<?php endif; ?>
		</div><!--.site-branding -->

		<nav id="site-navigation" class="site-navigation main-navigation" role="navigation">
			<div id="pull"><span class="togl">Menù</span> <i class="fa fa-bars"></i></div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
*/?>
